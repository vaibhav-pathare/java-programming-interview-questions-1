package coding.practice.sorting;

/**
 * 
 * @author Vaibhav M. Pathare
 * @Problem: Given an array A[] consisting 0s, 1s and 2s. The task is to write a
 *           function that sorts the given array. The functions should put all
 *           0s first, then all 1s and all 2s in last.
 *
 */
public class Segregate012 {
	/**
	 * functions will put all 0s first, then all 1s and all 2s in last
	 * 
	 * @param array
	 */
	public static void segregate012(Integer[] array) {
		int ptr0 = 0;
		int ptr1 = 0;
		int ptr2 = array.length - 1;

		while (ptr1 <= ptr2) {
			switch (array[ptr1]) {
			case 0:
				int temp = array[ptr0];
				array[ptr0] = array[ptr1];
				array[ptr1] = temp;
				ptr0++;
				ptr1++;
				break;

			case 1:
				ptr1++;
				break;

			case 2:
				temp = array[ptr2];
				array[ptr2] = array[ptr1];
				array[ptr1] = temp;
				ptr2--;
				break;
			}
		}
	}

	/**
	 * main function
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Integer[] array012 = { 0, 2, 1, 0, 1 };
		segregate012(array012);

		for (int no : array012) {
			System.out.print(no + " ");
		}
	}
}
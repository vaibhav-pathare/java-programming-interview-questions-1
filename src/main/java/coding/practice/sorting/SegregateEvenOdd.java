package coding.practice.sorting;

/**
 * 
 * @author: Vaibhav Pathare
 * @Problem: Given an array A[], write a function that segregates even and odd
 *           numbers. The functions should put all even numbers first, and then
 *           odd numbers.
 */
public class SegregateEvenOdd {
	/**
	 * functions will put all even numbers first, and then odd numbers
	 * 
	 * @param array
	 */
	public static void segregateEvenOdd(Integer[] array) {
		int eventPtr = 0;
		int oddPtr = array.length - 1;

		while (eventPtr <= oddPtr) {
			if (array[eventPtr] % 2 == 0)
				eventPtr++;

			if (array[oddPtr] % 2 != 0)
				oddPtr--;

			if (array[eventPtr] % 2 != 0 && array[oddPtr] % 2 == 0) {
				int temp = array[eventPtr];
				array[eventPtr] = array[oddPtr];
				array[oddPtr] = temp;
				eventPtr++;
				oddPtr--;
			}
		}
	}

	/**
	 * main function
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Integer[] array = { 12, 34, 45, 9, 8, 90, 3 };
		segregateEvenOdd(array);

		for (int no : array) {
			System.out.print(no + " ");
		}
	}
}
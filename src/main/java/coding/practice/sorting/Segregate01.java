package coding.practice.sorting;

/**
 * @author: Vaibhav M. Pathare
 * @Problem:You are given an array of 0s and 1s in random order. Segregate 0s on
 *              left side and 1s on right side of the array. Traverse array only
 *              once.
 *
 */
public class Segregate01 {
	/**
	 * function will segregate 0s on left side and 1s on right side of the array
	 * 
	 * @param array
	 */
	public static void segregate01(Integer[] array) {
		for (int fp = 0, lp = array.length - 1; fp < lp;) {
			if (array[fp] == 0)
				fp++;

			if (array[lp] == 1)
				lp--;

			if (array[fp] == 1 && array[lp] == 0) {
				int temp = array[fp];
				array[fp] = array[lp];
				array[lp] = temp;
				fp++;
				lp--;
			}
		}
	}

	/**
	 * main function
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Integer[] array = { 0, 0, 1, 0, 0 };
		segregate01(array);

		for (int no : array) {
			System.out.print(no + " ");
		}
	}
}